package br.com.itau.pinterest.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.pinterest.models.Perfil;

public interface PerfilRepository extends CrudRepository<Perfil, String>{

}
