package br.com.itau.pinterest.services;

import static org.mockito.ArgumentMatchers.any;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.itau.pinterest.data.FotoTransaction;
import br.com.itau.pinterest.models.Foto;
import br.com.itau.pinterest.models.FotoServiceResposta;
import br.com.itau.pinterest.models.FotoUsuario;
import br.com.itau.pinterest.repositories.FotoRepository;
import br.com.itau.pinterest.repositories.FotoUsuarioRepository;

public class FotoServiceTest {

	FotoService fotoService;
	
	FotoRepository fotoRepository;
	
	FotoUsuarioRepository fotoUsuarioRepository;
	
	Foto foto;
	FotoUsuario fotoUsuario;
	
	@Before
	public void preparar() {
		fotoService = new FotoService();
		fotoRepository = Mockito.mock(FotoRepository.class);
		fotoUsuarioRepository = Mockito.mock(FotoUsuarioRepository.class);
		
		fotoService.fotoRepository = fotoRepository;
		fotoService.fotoUsuarioRepository = fotoUsuarioRepository;
		
		foto = new Foto();
		foto.setEmail("teste@teste.com");
		
		fotoUsuario = new FotoUsuario();
		fotoUsuario.setEmailUsuario("teste@teste.com");
	}
	
	@Test
	public void testarInsercaoFoto() {
		Mockito.when(fotoRepository.save(foto)).thenReturn(foto);
		Mockito.when(fotoUsuarioRepository.save(any(FotoUsuario.class))).thenReturn(fotoUsuario);
		
		FotoTransaction fotoTransaction = fotoService.inserir(foto);
		Foto novaFoto = fotoTransaction.getFoto();
		FotoUsuario novoFotoUsuario = fotoTransaction.getFotoUsuario();
		
		assertNotNull(novaFoto.getId());
		assertNotNull(novaFoto.getDataPostagem());
		assertEquals("teste@teste.com", novoFotoUsuario.getEmailUsuario());
	}
}
